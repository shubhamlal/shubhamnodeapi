
const category = require('../api/controller/category');
const express =require('express');
const checkAuth = require('../api/Middleware/checkAuth')
const route = express.Router();





route.get("/", checkAuth,category.getAllCategory);
route.post('/',checkAuth,category.AddCategory); // api to create Category.
route.patch('/:id',checkAuth,category.UpdateCategory);
route.delete('/:id',checkAuth,category.deleteAnyOneCategory);




// api to get all Category.

module.exports= route;