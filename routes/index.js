// var express = require('express');
// var router = express.Router();

// /* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

// module.exports = router;


const express = require('express');
const router = express.Router();
const checkAuth = require('../api/Middleware/checkAuth')

const users = require('./users');
const category = require("./category")
const signIn = require("./signin")

router.use('/user',checkAuth,users);
router.use('/category',checkAuth,category);
router.use('/signIn',signIn);


module.exports = router;