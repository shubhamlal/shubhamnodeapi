// var express = require('express');
// var router = express.Router();

// /* GET users listing. */
// router.get('/', function(req, res, next) {
//   res.send('respond with a resource');
// });

// module.exports = router;

const user = require('../api/controller/users');
const express =require('express');
const checkAuth = require('../api/Middleware/checkAuth')

const route = express.Router();

route.post('/',checkAuth,user.addUser); //create user

route.get('/',checkAuth,user.readUser); //api to read user

route.get('/:id',checkAuth,user.ReadUserById); //api to read user by id

route.put('/:id',checkAuth,user.UpdateUser); //api to read user by id

route.delete('/:id',checkAuth,user.RemoveUser); //api to read user by id


module.exports= route;