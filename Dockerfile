FROM node:10.16.3

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install
# RUN npm uninstall bcrypt
RUN npm update
RUN npm install bcrypt@4.0.1
RUN npm update
RUN npm rebuild bcrypt --build-from-source

# Bun
# Create app directory
# WORKDIR /usr/src/app

# # Install app dependencies
# COPY package.json /usr/src/app/
# RUN npm install

# Bundle app source
COPY . /usr/src/app

EXPOSE 9000
CMD npm start
