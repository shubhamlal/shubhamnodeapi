
const models = require('../../models');
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;


exports.addUser = async(req,res)=>{
    const {firstName,middleName,lastName,mobile,emailId,password} = req.body;
    const {country_id, state_id, city_id} = req.body;
    // console.log(firstName);

    const emailExist = models.users.findAll({
        where:{
            email_id : emailId,
        }
    }).then( emailResult => {
        if(emailResult.length > 0){
            return res.status(409).json({
                message: "User Exist"
            })
        }else{
            const planTextPassword = password;
            var hashPassword = bcrypt.hash(planTextPassword, saltRounds).then(result => {
                const password = result
                let createdUser = models.users.
                create({firstName,middleName,lastName,mobile,emailId, status:true,password })
                .then( result => {
                    const userId = result.id;
                    let createdAddress = models.address.
                create({country_id, state_id, city_id, userId })
                .then( addressResult => {
                    res.status(200).json({ message: 'User Created' });
                });
                }).catch( error => {
                    res.status(422).json({ message: 'User Not Created' });
                });

            }).catch( error => {
                res.status(422).json({ message: 'User not created' });
            });
        }
    })
}

// Read User.
exports.readUser = async(req,res) =>{
    const{search,offset , pageSize } = paginationFUNC.paginationWithFromTo(req.query.search, req.query.from, req.query.to);
    const searchQuery = {
        [Op.or]:{
            firstName:{[Op.iLike]:search + '%'},
            middleName: { [Op.iLike]: search + '%' },
            lastName: { [Op.iLike]: search + '%' },
            emailId: { [Op.iLike]: search + '%' },
            mobile: sequelize.where(
                sequelize.cast(sequelize.col('users.mobile'), 'varchar'),
                { [Op.iLike]: search + '%' }
            )
        }
       
    }
    let userData = await models.users.findAll(
        {
            where:searchQuery,
           offset:offset,
           limit: pageSize
        }
    );
    let count = await models.users.findAll({
        where: searchQuery,
    });
    if (!userData) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        var i = 0;
        for(i; i < userData.length; i++){
            userData[i].password = null;
        }
        userData[0].password = null;
        res.status(200).json({
            data: userData,
            count: count.length
        });
    }
}

//Read User By ID
exports.ReadUserById = async(req , res)=>{
    const id = req.params.id;
    let userData = await models.users.readUserById(id);
    if(userData == 0){
        return res.status(404).json({ message: 'Data not found' });
    }
    let userAddress = await models.address.findAll({
        where:{
            userId: id,
        }
    })
    if(userAddress == 0){
        res.status(404).json({ message: 'Data not found' });
    }else{
        res.status(200).json({userData:userData,userAddress:userAddress});
    }
};

// Update User.
exports.UpdateUser = async (req, res) => {
    const id = req.params.id;
    const { firstName,middleName,lastName,mobile,emailId,customerId,password } = req.body;
    let updateData = await models.users.updateUser(id,firstName,middleName,lastName,mobile,emailId,customerId,password);
    if (updateData[0] === 0) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        // cache(id);
        res.status(200).json({ message: 'Success' });
    }
}

// Remove User.
exports.RemoveUser = async (req, res) => {

    const id = req.params.id;
    let deleteUser = await models.users.removeUser(id);
    if (!deleteUser[0]) {
        res.status(404).json({ message: 'Data not found' });
    } else {
        res.status(200).json({ message: 'Success' });
    }
}


