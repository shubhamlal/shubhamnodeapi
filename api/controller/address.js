
const models = require('../../models');
const paginationFUNC = require('../../utils/pagination'); // importing pagination function.
const sequelize = models.sequelize;
const Sequelize = models.Sequelize;
const Op = Sequelize.Op;


exports.addAddress = async (req, res) => {
    const { country_id, state_id, city_id, userId } = req.body;
    // const slug = categoryName.toLowerCase().split(" ").join("-");
    console.log(country_id, state_id, city_id)
    let createdAddress = await models.address.create({country_id, state_id, city_id});
    if (!createdAddress == 1) {
      res.status(422).json({ message: "Category not created" });
    } else {
      res.status(201).json(createdAddress);
    }
  };

  exports.getAllCategory = async(req,res) =>{

   
        const { search, offset, pageSize } = paginationFUNC.paginationWithFromTo(
          req.query.search,
          req.query.from,
          req.query.to
        );
        let CategoryData = await models.categories.findAll({
          where: {
            [Sequelize.Op.or]: {
                categoryName: { [Sequelize.Op.iLike]: search + "%" }
            },
            status: true,

          },
          
          offset: offset,
          limit: pageSize
        });
        let count = await models.categories.findAll({
          where: {
            [Sequelize.Op.or]: {
                categoryName: { [Sequelize.Op.iLike]: search + "%" }
            },
            status: true
          }
        });
          res.status(200).json({
            data: CategoryData,
            count: count.length
          });
  }


  // Remove Category.
exports.deleteAnyOneCategory = async (req, res) => {

  const id = req.params.id;
  console.log(id);
  let deleteCategory = await models.categories.destroy({
    where: {
      id: `${id}`
    }
  });
  console.log(deleteCategory);
  if (!deleteCategory == true) {
      res.status(404).json({ message: 'Data not found' });
  } else {
      res.status(200).json({ message: 'Success' });
  }
}



// Update Category.
exports.UpdateCategory = async (req, res) => {
  const id = req.params.id;
  const { categoryName,createdby } = req.body;
  const slug = categoryName.toLowerCase().split(" ").join("-");
  console.log(slug);
  let updateData = await models.categories.update({ categoryName, createdby, slug }, {
    where: {
      id : `${id}`
    }
  });
  if (updateData === 0) {
      res.status(404).json({ message: 'Data not found' });
  } else {
      // cache(id);
      res.status(200).json({ message: 'Success' });
  }
}



