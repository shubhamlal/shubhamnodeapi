
module.exports = (sequelize, DataTypes) => {
    const paymentMethod = sequelize.define('payment_methods', {
        // attributes
        payment_method: {
            type: DataTypes.STRING
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'payment_methods',
        });


    return paymentMethod;
}