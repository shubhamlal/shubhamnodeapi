module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
        // attributes
        
        firstName: {
            type: DataTypes.STRING,
            field:'first_name' 
        },
        lastName: {
            type: DataTypes.STRING,
            field:'last_name'
        },
        middleName: {
            type: DataTypes.STRING, 
            field:'middle_name' 
        },
        mobile: {
            type: DataTypes.BIGINT,
            field:'mobile' 
        },
        emailId: {
            type: DataTypes.STRING,
            field:'email_id' 
            
        },
        // customerId: {
        //     type: DataTypes.INTEGER,
        //     field:'customer_id' 

        // },
        // addressId:{
        //     type: DataTypes.INTEGER,
        //     field:'address_id' 
            
        // },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true,
            field:'status' 
        },
        password:{
            type:DataTypes.STRING,
            field:'password' 
        }
    }, {
            freezeTableName: true,
            tableName: 'users',
        });

        

        // defining Association
        users.associate = function(models) {
            // associations can be defined here
            users.hasMany(models.address, {
              foreignKey: 'userId',
              as: 'addresses',
              onDelete: 'CASCADE',
            });
        }







        users.readUserById=(id)=> users.findOne({where: { id, status: true }});   
        
        //Update_User 
        users.updateUser = async (id, firstName,middleName,lastName,mobile,emailId,password) => {
            let userData = await users.findOne({ where: { id: id, ststus: true } });
            if (userData) {
                let userUpdated = await users.update({ firstName,middleName,lastName,mobile,emailId,password}, { where: { id: id, status: true } });
                return userUpdated;
            }
        }
        //delete user
        users.removeUser = (id) => users.update({ status: false }, { where: { id: id, status: true } });

        return users;
}