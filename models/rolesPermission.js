
module.exports = (sequelize, DataTypes) => {
    const rolesPermissions = sequelize.define('roles_permissions', {
        // attributes
        permission_id:{
            type: DataTypes.INTEGER
        },
        roles_id:{
            type: DataTypes.INTEGER
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            tableName: 'roles_permissions',
        }); 
        
        return rolesPermissions;
    }