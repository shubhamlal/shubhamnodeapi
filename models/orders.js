
module.exports = (sequelize, DataTypes) => {
    const orders = sequelize.define('orders', {
        // attributes
        user_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        product_id: {
            type: DataTypes.INTEGER
        },
        quantity: {
            type: DataTypes.INTEGER
        },
        order_date: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        status: {
            type: DataTypes.STRING,
        },
        payment_type_id: {
            type: DataTypes.INTEGER
        },
        payment_method_id: {
            type: DataTypes.STRING
        },
        payment_ststus: {
            type: DataTypes.STRING
        },
        order_total_value: {
            type: DataTypes.FLOAT
        },
        branch_id: {
            type: DataTypes.INTEGER
        },
        partner_id: {
            type: DataTypes.INTEGER
        },
        emi_start_date: {
            type: DataTypes.DATE,
        },
        emi_end_date: {
            type: DataTypes.DATE,
        },
        comission:{
            type: DataTypes.FLOAT
        }

    }, {
            freezeTableName: true,
            tableName: 'orders',
        });

    return orders;
}