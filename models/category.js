const index =require("./index");

module.exports = (sequlize , DataType) =>{
    const category = sequlize.define("categories",
    {
        categoryName:{
            type:DataType.STRING,
            allowNill:false,
            defaultValue: 0,
            field:'category_name' 

        },
        // sku:{
        //     type:DataType.STRING,
        //     allowNill:false,
        //     defaultValue: 0,
        //     field:'sku' 
        // },
        
        slug:{
            type:DataType.STRING,
            allowNill:false,
            defaultValue: 0,
            field:'slug' 
        },
        status:{
            type:DataType.BOOLEAN,
            defaultValue: true
        },
        createdby:{
            type: DataType.INTEGER,
            defaultValue: 0
        }
        // createdAt: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        //     },
        // updated_at: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        // }
    },
    { 
       freezeTableName: true,
       tableName: "categories"
    }
    );

    category.createCategory = (categoryName,slug,createdby) =>
    category.create({
      categoryName,slug,createdby
    });

    return category;
}