const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const address = sequlize.define("address",
    {
        country_id:{
            type:DataTypes.INTEGER,
            defaultValue: 0
        },
        state_id:{
            type:DataTypes.INTEGER,
            defaultValue: 0
        },
        city_id:{
            type:DataTypes.INTEGER,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
        }
    },
    { 
       freezeTableName: true,
       tableName: "address"
    });

    return address;
}