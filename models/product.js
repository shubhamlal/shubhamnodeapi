
module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define('products', {
        // attributes
        category_id: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        sku: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0
        },
        product_name: {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 0
        },
        slug: {
            type: DataTypes.STRING
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue: true,
        },
        height: {
            type: DataTypes.FLOAT,
            defaultValue: 0
        },
        price: {
            type: DataTypes.FLOAT,
            defaultValue: 0
        },
        purity: {
            type: DataTypes.INTEGER,
            defaultValue: 0
        },
        product_image: {
            type: DataTypes.TEXT,
            defaultValue: 0
        },
        metatype: {
            type: DataTypes.TEXT,
            defaultValue: 0
        },
        thumbnail_image: {
            type: DataTypes.TEXT,
            defaultValue: 0
        },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0
        }
        // createdAt: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        //     },
        // updated_at: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        // }
       
    }, {
        freezeTableName: true,
        tableName: 'products',
    });

  
    return Product;
}