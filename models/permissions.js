
module.exports = (sequelize, DataTypes) => {
    const Permissions = sequelize.define('permissions', {
        // attributes
        name:{
            type: DataTypes.STRING
        },
         slug:{
            type:DataTypes.STRING,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            tableName: 'permissions',
        }); 
        
        return Permissions;
    }