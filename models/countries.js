const index =require("./index");

module.exports = (sequlize , DataTypes) =>{
    const countries = sequlize.define("countries",
    {
        country_name:{
            type:DataTypes.STRING,
            defaultValue: 0
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        ststus:{
            type:DataTypes.BOOLEAN,
            defaultValue: true
            },
            createdby:{
                type: DataTypes.TEXT,
                defaultValue: 0
            }
        // createdAt: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        //     },
        // updated_at: {
        //     type: DataTypes.DATE,
        //     defaultValue:DataTypes.NOW
        // }
    },
    { 
       freezeTableName: true,
       tableName: "countries"
    }
    );
    return countries;
}