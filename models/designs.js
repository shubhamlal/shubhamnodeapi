
module.exports = (sequelize, DataTypes) => {
    const designs = sequelize.define('designs', {
        // attributes
       front_size: {
            type: DataTypes.FLOAT
        },
        height: {
            type: DataTypes.FLOAT
        },
        width: {
            type: DataTypes.FLOAT
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'designs',
        });


    return designs;
}