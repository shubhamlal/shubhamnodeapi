
module.exports = (sequelize, DataTypes) => {
    const roleUsers = sequelize.define('roles_users', {
        // attributes
        role_id:{
            type: DataTypes.INTEGER
        },
        user_id:{
            type: DataTypes.INTEGER
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            roleUsers: 'roles_users',
        }); 
        
        return roleUsers;
    }