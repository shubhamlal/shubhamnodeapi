
module.exports = (sequelize, DataTypes) => {
    const paymentType = sequelize.define('payment_types', {
        // attributes
        payment_types: {
            type: DataTypes.STRING
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'payment_types',
        });


    return paymentType;
}