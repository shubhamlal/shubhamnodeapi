
module.exports = (sequelize, DataTypes) => {
    const Roles = sequelize.define('roles', {
        // attributes
        roleName: {
            type: DataTypes.STRING
        },
        slug:{
            type:DataTypes.STRING,
            allowNill:false,
            defaultValue: 0
        },
        status:{
            type:DataTypes.BOOLEAN,
            defaultValue:true
        },
        createdby:{
            type: DataTypes.TEXT,
            defaultValue: 0
        }
    }, {
            freezeTableName: true,
            allowNull: false,
            tableName: 'roles',
        });


    return Roles;
}